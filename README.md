# Bitbucket Pipelines Pipe: Fortify Scan

Runs a Fortify scan to scan your application for potential security vulnerabilities.

Build secure software fast with [Fortify](https://www.microfocus.com/en-us/cyberres/application-security). Fortify offers end-to-end application security solutions with the flexibility of testing on-premises and on-demand to scale and cover the entire software development lifecycle. With Fortify, find security issues early and fix at the speed of DevSecOps.

The Fortify Scan Pipe enables development teams to easily integrate static application security testing (SAST) and scan your application for potential security vulnerabilities. Additionally, users may configure the pipe to wait for scan completion and import the results into Code Insights, enabling full end-to-end security testing within the BitBucket ecosystem.

If you are not already a Fortify customer, check out our [Free Trial](https://www.microfocus.com/en-us/products/application-security-testing/free-trial).

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: fortifysoftware/fortify-scan:3.2.9-jdk-11
  variables:
    PACKAGE_OPTS: -bt mvn
    FOD_URL: $FOD_BASE_URL
    FOD_TENANT: $FOD_TENANT
    FOD_USER: $FOD_USER
    FOD_PAT: $FOD_PAT
    FOD_RELEASE_ID: $FOD_ID
    FOD_UPLOAD_OPTS: -ep 1
```

## Variables

| Variable           | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| **Control Options**| |
| `DO_PACKAGE` | Packaging is enabled by default, but can be disabled if a package has been generated through other means, or if you're not actually starting a scan (see DO_SCAN below). |
| `DO_SCAN` | Running a scan is enabled by default, but can be disabled. Setting this to false usually only makes sense if you want to test packaging and/or export steps without actually running a scan. |
| `DO_EXPORT` | Exporting is enabled by default if EXPORT_TARGET has been defined (see below). Setting this to false allows for skipping the export even though an export target has been defined. |
| `DO_BLOCK` | Block until scan completion is disabled by default, unless exporting has been enabled (in which case blocking cannot be disabled). Setting this option to true allows for enabling blocking until scan completion, for example allowing to process scan results through some other means, when exporting hasn't been enabled. |
| **Packaging Options** | |
| `PACKAGE_NAME` | Specify the file containing the scan payload. The packaging process will write the scan payload to this file, and the scanning process will read the scan payload from this file. Defaults to package.zip in the current working directory. |
| `PACKAGE_OPTS` | Specify packaging options that will be passed to the scancentral command. See the ScanCentral documentation for more information about packaging options. As an example, you can use this option to specify '-bt mvn' or '-bt gradle'. This option doesn't have a default value, and is required if DO_PACKAGE hasn't been set to false. |
| **Export Options** | |
| `EXPORT_TARGET` | Specify the target for vulnerability data export. This will be used to generate the configuration file name passed to FortifyVulnerabilityExporter, in the format `SSCTo<ExportTarget>` or `FoDTo<ExportTarget>`. As an example, if `EXPORT_TARGET` has been set to GitLabSAST and an FoD scan is being run, FortifyVulnerabilityExporter will be invoked with the FoDToGitLabSAST configuration. See the FortifyVulnerabilityExporter documentation (or the list of configuration files shipped with FortifyVulnerabilityExporter) for an overview of available export targets. Some examples: BitBucket, GitHub, GitLab, GitLabSAST, SonarQube. |
| **FoD Options** | |
| `FOD_URL` | Specify the FoD portal URL, for example https://ams.fortify.com. Required for running scans on, and exporting vulnerability data from FoD. |
| `FOD_API_URL` | This script automatically determines the API URL based on FOD_URL, but can be overridden if necessary. |
| `FOD_TENANT` | Specify the FoD tenant. Required for running scans on, and exporting vulnerability data from FoD. |
| `FOD_USER` or `FOD_USERNAME` | Specify the FoD user name. Required when connecting to FoD using user credentials. |
| `FOD_PASSWORD` or `FOD_PAT` | Specify the FoD password or PAT. Required when connecting to FoD using user credentials. |
| `FOD_CLIENT_ID` | Specify the FoD client id. Required when connecting to FoD using client/API credentials. |
| `FOD_CLIENT_SECRET` | Specify the FoD client secret. Required when connecting to FoD using client/API credentials. |
| `FOD_RELEASE_ID` | Specify the FoD release id. Required for running scans on, and exporting vulnerability data from FoD. |
| `FOD_NOTES` | Specify optional scan notes to be passed to FoD. |
| `FOD_UPLOAD_OPTS` | Specify any additional options for FoDUploader; see FoDUploader documentation for a list of available upload options. FoDUploader requires at least the '-ep' option to be passed. Note that any relevant FoD options listed above are automatically passed to FoDUploader. In addition, the following options are automatically passed to FoDUploader under certain conditions: <ul><li>`-I 1`: passed if `DO_BLOCK` or `DO_EXPORT` are set to true</li><li>`-apf`: passed if `DO_EXPORT` is set to true</li></ul> |
| **On-premises Options** | |
| `SSC_URL` | Specify the SSC base URL, for example https://my.ssc.host/ssc. Required for running scans on ScanCentral SAST, and for exporting vulnerability data from SSC. |
| `SSC_CI_TOKEN` | Specify the SSC CIToken used for authenticating with SSC. Required for running scans on ScanCentral SAST, and for exporting vulnerability data from SSC. |
| `SSC_VERSION_ID` | Specify the SSC application version id. Required for running scans on ScanCentral SAST, and for exporting vulnerability data from SSC. |
| `SSC_PROCESSING_WAIT_TIME` | Specify number of seconds (s), minutes (m) or hours (h) to wait after ScanCentral has completed the scan, in order to allow for SSC to finish processing the scan results. At the moment, this script nor any of the standard Fortify CLI tools provide functionality to poll SSC for processing completion, or to check whether an artifact has been processed successfully. As a result, for now this script only supports a static wait time. This setting is only applicable if blocking is enabled (see DO_BLOCK). Default value is 5m. |
| `SCANCENTRAL_OPTS` | Specify any additional options for the 'scancentral start' command; see ScanCentral documentation for a list of available options. Note that any relevant SSC options listed above are automatically passed to the `scancentral start` command. In addition, the following options are automatically passed to the `scancentral start` command under certain conditions: <ul><li>`-block`: passed if `DO_BLOCK` or `DO_EXPORT` are set to true</li></ul> |

## Prerequisites

To run FoD scans, you will need to have an account on Fortify on Demand. To run on-premises scans, you will need to have access to a Fortify Software Security Center (SSC) and Fortify ScanCentral SAST environment.

## Examples

### FoD - Basic Scan

Following is an example of a partial BitBucket pipeline that runs a scan on Fortify on Demand and reports results back to BitBucket.

```yaml
pipelines:
  default:
    - step:
        script:
          - pipe: fortifysoftware/fortify-scan:3.2.9-jdk-11
            variables:
              PACKAGE_OPTS: -bt mvn
              FOD_URL: $FOD_BASE_URL
              FOD_TENANT: $FOD_TENANT
              FOD_USER: $FOD_USER
              FOD_PAT: $FOD_PAT
              FOD_RELEASE_ID: $FOD_RELEASE_ID
              FOD_UPLOAD_OPTS: -ep 1
```

To run an asynchronous scan without reporting results back to BitBucket, you can pass the `DO_EXPORT: false` variable, in which case the pipeline will terminate as soon as the scan request has been submitted to Fortify on Demand.

### On-premises - Basic Scan

Following is an example of a partial BitBucket pipeline that runs a scan on Fortify ScanCentral SAST and reports results back to BitBucket.

```yaml
pipelines:
  default:
    - step:
        script:
          - pipe: fortifysoftware/fortify-scan:3.2.9-jdk-11
            variables:
              PACKAGE_OPTS: -bt mvn
              SSC_URL: $SSC_BASE_URL
              SSC_CI_TOKEN: $SSC_CI_TOKEN
              SSC_VERSION_ID: $SSC_VERSION_ID
```

To run an asynchronous scan without reporting results back to BitBucket, you can pass the `DO_EXPORT: false` variable, in which case the pipeline will terminate as soon as the scan request has been submitted to the Fortify ScanCentral SAST Controller.

### Advanced Scans

The ability to run a Fortify scan using the Fortify Scan Pipe may not be a perfect fit in all scenarios. Following are some scenarios in which you may need to apply a different approach:
* Scanning .NET applications or other applications that require a Windows platform for building or translating the application
* The build tools or build tool versions shipped with the Fortify Scan Pipe are incompatible with the application to be scanned
* The ScanCentral Client shipped with the Fortify Scan Pipe is not compatible with the Fortify ScanCentral SAST instance used for running the scans
* You require a different (base) image for building or scanning your application
* You want full control over the various Fortify commands

In such cases you can utilize [FortifyToolsInstaller](https://github.com/fortify/FortifyToolsInstaller) to install the individual Fortify tools, followed by scripted invocation of those individual Fortify tools. Following is an example:

```
pipelines:
  default:
    - step:
        image: maven:3.8.1-jdk-11
        script:
           - |
              export FTI_VERSION=v2.1.0 FTI_SHA256=05ac617d1e6fde80caa45fa7a1300d34cbd30a714c5276db96cc04876e7646b6 \
              && curl -fsSL https://raw.githubusercontent.com/fortify/FortifyToolsInstaller/${FTI_VERSION}/FortifyToolsInstaller.sh -o /tmp/FortifyToolsInstaller.sh \
              && echo -n "${FTI_SHA256} /tmp/FortifyToolsInstaller.sh" | sha256sum --check - \
              && FTI_TOOLS=ScanCentral:21.1.2,FoDUpload:v5.2.1,FortifyVulnerabilityExporter:v1.5.0 source /tmp/FortifyToolsInstaller.sh \
              && scancentral package -bt mvn -o package.zip \
              && FoDUpload -z package.zip -aurl "$FOD_API_URL" -purl "$FOD_BASE_URL" -rid "$FOD_RELEASE_ID" -tc "$FOD_TENANT" -uc "$FOD_USER" "$FOD_PAT" -ep 2 -pp 0 -I 1 -apf -n "Submitted from BitBucket Pipeline" \
              && FortifyVulnerabilityExporter FoDToBitBucket --fod.baseUrl=$FOD_BASE_URL --fod.tenant=$FOD_TENANT --fod.userName=$FOD_USER --fod.password=$FOD_PAT --fod.release.id=$FOD_RELEASE_ID
           - |
            curl --proxy 'http://localhost:29418' --request PUT "http://api.bitbucket.org/2.0/repositories/$BITBUCKET_REPO_OWNER/$BITBUCKET_REPO_SLUG/commit/$BITBUCKET_COMMIT/reports/test-001" --header 'Content-Type: application/json' -d @bb-fortify-report.json \
            && curl --proxy 'http://localhost:29418' --request POST "http://api.bitbucket.org/2.0/repositories/$BITBUCKET_REPO_OWNER/$BITBUCKET_REPO_SLUG/commit/$BITBUCKET_COMMIT/reports/test-001/annotations" --header 'Content-Type: application/json' -d @bb-fortify-annotations.json
```

The first script part installs the various Fortify tools and runs them. The second script part uploads the BitBucket Code Insights reports generated by FortifyVulnerabilityExporter to BitBucket.

## Support
If you'd like help with this pipe, or you have an issue or feature request, please submit a Fortify support request or log into Fortify on Demand to chat with one of our support agents.

If you're reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce

## License
Copyright (c) 2021 Micro Focus
MIT licensed, see [LICENSE.txt](LICENSE.txt) file.
